# dpublico.partidopirata.com.ar

web del día del dominio público

## compilar

para compilar necesitas [svgo](https://github.com/svg/svgo) instalado

y después corres `make`

## deploy (subirlo al servidor)

```
rsync -rvzP dist/ root@partidopirata.com.ar:/srv/http/dpublico.partidopirata.com.ar/
```

